provider "aws" {
  region  = "us-east-1"
  version = "~> 2.0"
}

terraform {
  required_version = ">= 0.12"

  backend "s3" {
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    bucket         = "bixbots-terraform-state"
    key            = "test/management/instance"
    encrypt        = "true"
  }
}
