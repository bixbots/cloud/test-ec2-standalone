locals {
  region      = "us-east-1"
  application = "instance"
  environment = "management"
  role        = "test"
  lifespan    = "temporary"

  spot_price    = "0.0052"
  instance_type = "t3.nano"

  subnet_type = "private"
  key_name    = "platform-management"

  tags = {}
}

data "aws_ami" "primary" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "tags_base" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = local.application
  environment = local.environment
  lifespan    = local.lifespan
  tags        = local.tags
}

module "launch_pad" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git?ref=v1"

  region      = local.region
  application = local.application
  environment = local.environment
  role        = local.role

  enable_admin_access = true
  allow_all_egress    = true

  tags = module.tags_base.tags
}

module "cloud_init_dns" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/cloud-init-dns?ref=v1"

  region      = local.region
  application = local.application
  environment = local.environment
  role        = local.role
  subnet_type = local.subnet_type
  iam_role_id = module.launch_pad.iam_role_id
}

module "standalone_ec2" {
  source = "git::ssh://git@gitlab.com/bixbots/cloud/terraform-modules/standalone-ec2?ref=v1"

  region      = local.region
  name        = module.launch_pad.name
  vpc_id      = module.launch_pad.vpc_id
  subnet_type = local.subnet_type

  spot_price    = local.spot_price
  instance_type = local.instance_type
  image_id      = data.aws_ami.primary.id
  key_name      = local.key_name

  security_group_ids      = module.launch_pad.security_group_ids
  iam_instance_profile_id = module.launch_pad.iam_instance_profile_id

  rendered_cloud_init = [module.cloud_init_dns.part]

  tags = module.launch_pad.tags
}
